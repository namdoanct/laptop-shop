<!DOCTYPE html>
<html>
	<?php include_once('head.php');?>
	<body>
	<?php include_once('banner.php');?>
		<div id="container">
			
			<div class="contein">
				<?php include_once('left.php');?>
				<div class="right">
					<br>
					<table class="cart2">
						<tr>
							<th>
								GIỚI THIỆU Ý TƯỞNG THIẾT KẾ WEB
							</th>
						</tr>
						<tr>
							<td style="padding: 10px;">
								<p>
									<a href="#" style="font-weight: bold; padding-left: 30px;">C</a>húng ta có thể nói rằng thế ký 21 đã và đang chứng kiến sự phát triển mạnh mẽ của ngành Công nghệ thông tin (CNTT). CNTT đã và đang thay đổi thế giới một cách nhanh chóng và tưng bước kéo nền tri thức của nhân loại xích lại gần nhau hơn. Đó là nhờ việc phát triển hệ thống website trên toàn thế giới.
								</p>
								<p>
									<a href="#" style="padding-left: 30px;">N</a>gày nay, Website đã đóng một vai trò quan trọng đối với con người chúng ta từ giải trí cho đến quảng cáo, thương mại, quản lý…Website(thương mại điện tử) sẽ dần thay thế những phương thức kinh doanh cũ trong các doanh nghiệp bởi tính ưu việt mà Website mang lại như: nhanh hơn, rẻ hơn, tiện dụng hơn, hiệu quả hơn và không bị giới hạn không gian và thời gian.
								</p> 
								<p>
									<a href="#" style="padding-left: 30px;">Đ</a>ể góp phần thúc đẩy thương mại điện tử ở Việt Nam, nhóm em đã tìm hiểu và thực hiện đề tài “ Xây dựng website bán laptop”
								</p>

							</td>
						</tr>
					</table>
					<br>
					<table class="cart" >
						<tr>
							<th colspan="2" style="background: #e47c11; color: white; padding: 5px;">Nhóm phát triển WEB</th>
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px; width: 300px">Ý tưởng thiết kế:</td>
							<td>Mai Đức Tin , Doãn Văn Nam</td>
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px;">HTML + CSS + JAVASCRIPT:</td>
							<td>Mai Đức Tin , Phạm Anh</td>
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px;">PHP:</td>
							<td>Mai Đức Tin </td>
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px;">Thông số kỹ thuật và hình ảnh:</td>
							<td>Nguyễn Minh Toàn , Phan Quang Thắng, Doãn Văn Nam</td>	
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px;">Cơ sở dữ liệu (MYSQL):</td>
							<td>Phan Quang Thắng, Doãn Văn Nam, Nguyễn Minh Toàn</td>
						</tr>
						<tr>
							<td style="font-weight: bold; padding-left: 15px;">TESTER:</td>
							<td>Tạ Ngọc Trọng, Phạm Anh</td>
						</tr>
					</table>
					<hr><br><hr>
					<table class="cart">
						<tr>
							<th  colspan= "2" style="width: 200px">Giáo viên hướng dẫn:</th>
							<th colspan="2">Đoàn Duy Bình</th>	
						</tr>
						<tr>
							<th colspan="4" style="background: #e47c11; color: white; padding: 5px;">THÀNH VIÊN THỰC HIỆN</th>
						</tr>
						<tr>
							<th style="width: 50px">STT</th>
							<th style="width: 150px">HỌ VÀ TÊN</th>
							<th style="width: 50px;">NHÓM TRƯỞNG</th>
							<th>CÔNG VIỆC</th>
						</tr>
						<tr>
							<th>1</th>
							<td>Mai Đức Tin</td>
							<th>X</th>
							<td>Chọn ý tưởng, trình bày báo cáo.</td>
						</tr>
						<tr>
							<th>2</th>
							<td>Phan Quang Thắng </td>
							<td/>
							<td>Lên ý tưởng, code cơ sở dữ liệu.</td>
						</tr>
						<tr>
							<th>3</th>
							<td>Nguyễn Minh Toàn</td>
							<td/>
							<td>Lên ý tưởng và thiết lập Admin.</td>
						</tr>
						<tr>
							<th>4</th>
							<td>Tạ Ngọc Trọng</td>
							<td/>
							<td>Lên ý tưởng và thiết lập account mới , dăng nhập và đăng xuất.</td>
						</tr>
						<tr>
							<th>5</th>
							<td>Doãn Văn Nam </td>
							<td/>
							<td>Lên ý tưởng thiết kế và thiết lập tìm kiếm sản phẩm</td>
						</tr>
						<th>6</th>
							<td>Phạm Anh</td>
							<td/>
							<td>Lên ý tưởng , quản lí danh sách sản phẩm và thông tin liên lạc</td>
						</tr>
					</table><br>
				</div>
			</div>
			<?php include_once('footer.php');?>
		</div>
	</body>
</html>