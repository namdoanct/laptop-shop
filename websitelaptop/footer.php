<div class="clear"></div>
<div class=" footer-up" >
	<div class="colum colum1">
		<ul>

				<h2>Về chúng tôi</h2>

			<li>
				<p style="color:#fff;">Ngày nay, Website đã đóng một vai trò quan trọng đối với con người chúng ta từ giải trí cho đến quảng cáo, thương mại, quản lý…Website(thương mại điện tử) sẽ dần thay thế những phương thức kinh doanh cũ trong các doanh nghiệp bởi tính ưu việt mà Website mang lại như: nhanh hơn, rẻ hơn, tiện dụng hơn, hiệu quả hơn và không bị giới hạn không gian và thời gian</p>
			</li>

		</ul>
	</div>
	<div class="colum colum2" >
		<ul>

				<h2>Đường dẫn</h2>

			<li>
				<a href="home.php" style="color:#fff">Trang chủ</a>
			</li>
			<li>
				<a href="gioithieu.php" style="color:#fff">Giới thiệu</a>
			</li>
			<li>
				<a href="home.php" style="color:#fff">Sản phẩm</a>
			</li>
			<li>
				<a href="lienhe.php" style="color:#fff">Liên hệ</a>
			</li>
			<li>
				<a href="timkiem.php" style="color:#fff">Tìm kiếm</a>
			</li>
		</ul>
	</div>
	<div class="colum">
		<ul>

				<h2>Thông tin liên hệ</h2>

				<li>
					<span><i class="fa fa-map-marker"></i></span>
                     <p style="color:#fff">Đường Tôn Đức Thắng<br />
                         Quận Liên Chiểu , Thành Phố Đà Nẵng<br />
                         Việt Nam</p>
                 </li>
                 <li>
				 <span><i class="fa fa-map-marker"></i></span>
                     <p><a href="#" style="color:#fff">+84 0403 0507</a>
                         <br />
                         <a href="#" style="color:#fff">+84 3939 8686</a></p>
                 </li>
                 <li>
				 <span><i class="fa fa-map-marker"></i></span>
                     <p><a href="#" style="color:#fff">manguonmonhom2@gmail.com</a></p>
                </li>
		</ul>
	</div>
</div>
<div class="footer" style="background:#000000d4">
	<address>Copyright 2021 ©manguonmo, Inc. all rights reserved
	</address>
</div>